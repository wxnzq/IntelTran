/**
 * @author NZQ
 * @data 2018/8/29
 * @Description : 表格生成
 * @param: obj.elem     jquery获取的元素
 * @param: obj.columns  行配置
 * @param: obj.data     表格数据
 */
function createTable (obj) {
    let elem = obj.elem;
    let columns = obj.columns;
    let data = obj.data;

    let elemWrap = elem.parent();
    let height = elemWrap.height();
    let width = elemWrap.width();
    $(".fixed-table-container").height = height;

    elem.bootstrapTable('destroy');

    elem.bootstrapTable({
        data: data,
        classes: 'table table-hover',
        height: height,
        //url: queryUrl,                      //请求后台的URL（*）
        //method: 'GET',                      //请求方式（*）
        //dataType : "json",
        striped: true,                      //是否显示行间隔色
        cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true,                   //是否显示分页（*）
        sortable: true,                     //是否启用排序
        sortOrder: "asc",                   //排序方式
        sidePagination: "client",           //分页方式：client客户端分页，server服务端分页（*）
        //pageNumber: 1,                      //初始化加载第一页，默认第一页,并记录
        pageSize: 20,                     //每页的记录行数（*）-- queryParams
        pageList: [20, 50, 100],        //可供选择的每页的行数（*）
        search: false,                      //是否显示表格搜索
        strictSearch: false,
        showColumns: false,                  //是否显示所有的列（选择显示的列）
        showRefresh: false,                  //是否显示刷新按钮
        minimumCountColumns: 2,             //最少允许的列数
        clickToSelect: false,                //是否启用点击选中行
        // singleSelect : true, // 只能单选
        uniqueId: "fid",                     //每一行的唯一标识，一般为主键列 -- 对应列中的 key
        showToggle: false,                   //是否显示详细视图和列表视图的切换按钮
        cardView: false,                    //是否显示详细视图
        detailView: false,                  //是否显示父子表
        // responseHandler: ioms_responseHandler,//加载服务器数据之前的处理程序，可以用来格式化数据。
        //得到查询的参数
        //queryParams: function (params) {
        //    //这里的键的名字和控制器的变量名必须一致，这边改动，控制器也需要改成一样的
        //    let temp = {
        //        rows: params.limit,                         //页面大小
        //        page: (params.offset / params.limit) + 1,   //页码
        //        sort: params.sort,      //排序列名
        //        sortOrder: params.order //排位命令（desc，asc）
        //    };
        //    return temp;
        //},
        columns: columns,
        onLoadSuccess: function () {
        },
        onLoadError: function () {
            showTips("数据加载失败！");
        },
        onClickRow: function (row, $element) {
            return 1;
            /*let featureid = row.fid;
            let realval = row.realval;
            if (realval == 1) {
                realval = 0;
            } else {
                realval = 1;
            }
            updatePlanVal(featureid, realval);*/
        },
        onDblClickRow: function (row, $element) {
            let id = row.ID;
            EditViewById(id, 'view');
        },
        onPageChange : function (num , page) {
        }
    });

    /*
    elem.on('post-body.bs.table', function (e) {//渲染完成，重新设置高度
        let h = elem.height();
        if (h > 500) {
            elem.bootstrapTable('resetView', { 'height': 300 });
        }
    });*/

    $(window).resize(function () {
        elem.bootstrapTable('resetView');
    });
}

/**
 * @author NZQ
 * @data 2018/8/29
 * @Description : 表格更新
 * @param: obj.elem     query获取的元素
 * @param: obj.data     表格数据
 */
function updateTable (obj) {
   obj.elem.bootstrapTable('load', obj.data);//主要是要这种写法
}


export { createTable, updateTable }