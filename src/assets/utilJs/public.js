/**
 * @author NZQ
 * @date 2018/8/22nzq.json
 * @Description : 一些重要的公共函数
*/


/**
 * @author NZQ
 * @date 2018/8/25
 * @Description : 模拟jquer选择器
 * @Param :
*/
/*const $ = function (selector) {
    if (selector[0] === '#') {
        return document.querySelector(selector)
    } else {
        return document.querySelectorAll(selector)
    }
}*/

/**
 * @author NZQ
 * @date 2018/8/25
 * @Description : 模板插入
 * @Param : template ,html模板
 * @Param : insertedDOM ,待插入元素的节点
*/
const insertTemplate = function (template, insertedDOM) {
    let DOM = document.createElement('div')
    DOM.innerHTML = template
    insertedDOM.append(DOM)
};



/**
 * @author NZQ
 * @data 2018/8/24
 * @Description : ajax 封装,会在引入public的时候自动加载
 *type               请求的方式  默认为get
 * url               发送请求的地址
 * data             发送请求的参数
 * contentType       客户端发送数据类型
 * isShowLoader      是否显示loader动画  默认为false
 * dataType          返回数据类型  默认为JSON格式数据
 * sucCallback       请求成功的回调函数
 * errorCallback     请求错误的回掉函数
 * timeout           超时时长,默认5秒
 * https://www.cnblogs.com/tnnyang/p/5742199.html (thanks)
 */


///////////////////////// 使用时用 new AjaxRequest() , 做请求 ，这里里面传入上面注释中的数据和一些东西就可以了
    /*
  如  new AjaxRequest({
          type : "POST",
          "sucCallback" : function(成功数据){

          }
     })


     还有就是  成功或者错误的函数  可以自己定义
     比如： function try1(data){
        console.log（data）
     }
     "sucCallback" : try1
    * */
const ajax = function () {
    function AjaxRequest (opts) {
        this.type = opts.type;
        this.url = "http://localhost:8080/" + opts.url;
        this.data = opts.data || {};
        this.contentType = opts.contentType || "application/json";
        this.isShowLoader = opts.isShowLoader || false;
        this.dataType = opts.dataType || "json";
        this.sucCallback = opts.sucCallback;
        this.errorCallback = opts.errorCallback;
        this.timeout = opts.timeout || 5000;
        this.sendRequest();
    }

    AjaxRequest.prototype = {
        // 渲染loader
        showLoader : function () {

        },
        hideLoader : function () {

        },
        sendRequest : function () {
            let self = this ;
            $.ajax({
                type : this.type,
                url : this.url,
                async : true,
                contentType : this.contentType,
                data : this.param,
                dataType : this.dataType,
                beforeSend : this.showLoader(),
                timeout : this.timeout,
                success : function (res) {
                    if (res !== null && res !== "") {
                        if (self.sucCallback) {
                            //Object.prototype.toString.call方法--精确判断对象的类型
                            if (Object.prototype.toString.call(self.sucCallback) === "[object Function]") {
                                self.sucCallback(res);
                            }else {
                                console.log("callBack is not a function");
                            }
                        }
                    }
                },
                error : function () {
                    if (self.errorCallback) {
                        //Object.prototype.toString.call方法--精确判断对象的类型
                        if (Object.prototype.toString.call(self.errorCallback) === "[object Function]") {
                            self.errorCallback();
                        }else {
                            console.log("callBack is not a function");
                        }
                    }
                },
                complete : function(XMLHttpRequest,status){ //请求完成后最终执行参数
                    if(status === 'timeout'){//超时,status还有success,error等值的情况
                        alert("超时");
                    }
                }
            })
        }
    }

    window.AjaxRequest = AjaxRequest;
}


/**
 * @author NZQ
 * @date 2018/9/1
 * @Description : 用于左侧导航栏导航,点击不同的li contentWrap更换不同的内容
 * @Param : nav 左侧d导航栏的li 数组 -- 共有特征 函数里面赋值
 * @Param : obj 对应于 左侧导航栏的方程 和  key
 * @Param : contentWrap 存放 不同nav下面的内容 -- 共有特征 函数里面赋值
 * @Param : page 当前页面
*/
function Nav (page, obj) {
    /*for (let key in obj) {}*/
    // 左侧导航栏
    this.nav = $(".left-nav ul li");
    this.contentWrap = $(".content-wrap");
    this.obj = obj;
    this.page = page;
    this.realNav();
}
Nav.prototype.realNav = function () {
    let that = this;
    for (let i = 0, length = that.nav.length; i < length; i++) {
        $(that.nav[i]).click(function () {
            let url = $(this).data().url;
            that.contentWrap.empty();
            insertTemplate(require(`../../page/${that.page}/components/${url}.html`), that.contentWrap);
            if (url in that.obj) {
                that.obj[url]();
            }
        })
    }
}

export { insertTemplate, ajax, Nav }