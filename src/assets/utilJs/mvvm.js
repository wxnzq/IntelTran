/**
 * Created by 王冬 on 2017/12/14.
 */
/**
 * @author NZQ
 * @date 2018/8/22
 * @Description :  来源于上述作者
*/

// 这是一个简陋的双向绑定
// DOM需要是jQuery的DOM（为了省事）

/**
 * @author NZQ
 * @date 2018/8/25
 * @Description :
 * @Param : obj,参数对象
 * @Param : key，参数名
 * @Param : $DOM， 节点
*/
const mvvm = function (obj, key, $DOM) {
    // prop() 方法设置或返回被选元素的属性和值。
    if ($DOM.prop('tagName') === "INPUT") {
        $DOM.keyup(function () {
            obj[key] = $DOM.val()
        })
    }
    let state = Object.getOwnPropertyDescriptor(obj, key)
    Object.defineProperty(obj, key, {
        configurable: true,
        set(newVal) {
            let val
            let isInput
            if (/input|textarea/i.test($DOM.prop('tagName'))) {
                val = $DOM.val()
                isInput = true
            } else {
                val = $DOM.text()
                isInput = false
            }
            if (newVal !== val) {
                if (isInput) {
                    $DOM.val(newVal)
                } else {
                    $DOM.text(newVal)
                }
                if (state && typeof state.get === 'function') {
                    state.get.call(null, newVal)
                }
            }
        },
        get() {
            if (/input|textarea/i.test($DOM.prop('tagName'))) {
                return $DOM.val()
            } else {
                return $DOM.text()
            }
        }
    })
}
export default mvvm