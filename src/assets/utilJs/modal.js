/**
 * @author NZQ
 * @data 2018/9/1
 * @Description : 模态框相关的关闭 等等操作
 */

function closeModal (elem) {
    elem.css({
        display : "none"
    })
}

function openModal (elem) {
    elem.css({
        "z-index" : 999,
        display : "block"
    })
}

/**
 * @author NZQ
 * @date 2018/9/8
 * @Param : modal 弹窗元素
 * @Param : openElem 点击该元素打开弹窗
 * @Param : closeElem 点击该元素关闭弹窗
 * @Param : modalTitle 弹窗题目
*/
// 模块处理
function modalHandle (modal, openElem, closeElem, modalTitle) {
    openElem.click(function () {
        openModal(modal);
        $(".modal-header h4").text(modalTitle);
        closeElem.click(function () {
            closeModal(modal);
        })
    })
}

export { closeModal, openModal, modalHandle }