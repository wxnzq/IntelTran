/**
 * @author NZQ
 * @date 2018/8/22
 * @Description : 单项绑定
*/

// DOM需要是jQuery的DOM（为了省事）
const sigleBinding = {
    // 绑定文本框
    // obj --- 要在其上定义属性的对象。
    // key --- 要定义或修改的属性的名称。
    // $textArea --- 所选择的文本框
    textArea(obj, key, $textArea) {
        Object.defineProperty(obj, key, {
            configurable: true,
            set(newVal) {
                $textArea.text(newVal)
            },
            get() {
                return $textArea.text()
            }
        })
    },

  // 绑定输入框
    input(obj, key, $input) {
        Object.defineProperty(obj, key, {
            configurable: true,
            set(newVal) {
                $input.val(newVal)
            },
            get() {
                return $input.val()
            }
        })
    }
}

export default sigleBinding