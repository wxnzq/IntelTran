/**
 * @author NZQ
 * @date 2018/8/22
 * @Description : less的一系列的初始化样式
*/

const globalVars = {
    // 样例
    blue: '#00AAFF',
    greyFont: '#3D474D',

    //nzq
    // 字
    fontWhite : "#FFFFFF",
    fontBlack : "#434343",
    fontLightGray : "#B3B3B3",
    fontGray : "#606266",
    fontFamily : '"Source Sans Light", "Apple LiSung Light", "STHeiti Light",SimSun, sans-serif;',
    font11 : '11px "Source Sans Light", "Apple LiSung Light", "STHeiti Light",SimSun, sans-serif;',
    font12 : '12px "Source Sans Light", "Apple LiSung Light", "STHeiti Light",SimSun, sans-serif;',
    font13 : '13px "Source Sans Light", "Apple LiSung Light", "STHeiti Light",SimSun, sans-serif;',
    font14 : '14px "Source Sans Light", "Apple LiSung Light", "STHeiti Light",SimSun, sans-serif;',
    font15 : '15px "Source Sans Light", "Apple LiSung Light", "STHeiti Light",SimSun, sans-serif;',
    font16 : '16px "Source Sans Light", "Apple LiSung Light", "STHeiti Light",SimSun, sans-serif;',
    font17 : '17px "Source Sans Light", "Apple LiSung Light", "STHeiti Light",SimSun, sans-serif;',
    font18 : '18px "Source Sans Light", "Apple LiSung Light", "STHeiti Light",SimSun, sans-serif;',
    font19 : '19px "Source Sans Light", "Apple LiSung Light", "STHeiti Light",SimSun, sans-serif;',
    font20 : '20px "Source Sans Light", "Apple LiSung Light", "STHeiti Light",SimSun, sans-serif;',
    font21 : '21px "Source Sans Light", "Apple LiSung Light", "STHeiti Light",SimSun, sans-serif;',
    font22 : '22px "Source Sans Light", "Apple LiSung Light", "STHeiti Light",SimSun, sans-serif;',
    fontWeight6 : 600,
    //背景
    bkWhite : "#FFF",
    bkLightGray : "#F2F2F2",
    bkBlue : "#025B97",
    bkHoverBlue : "#027BCC",
    bkSkyBlue : "#76b5ce",
    bkGray : "#e2e2e2",
    // 线
    lineGray : "1px solid #D2D2D2",
    popLineGray : "1px solid #B9B9B9",
    lineBlue : "1px solid #025B97",
    borderRadius2 : "2px",
    borderRadius3 : "3px",
    borderRadius5 : "5px",
    borderRadius10 : "10px",
    // display
    XCenter : "0 auto;",
    height20 : "20px",
    height30 : "30px",
    height40: "40px",
    height50 : "50px",
    height60 : "60px",
    // z-index
    zIndex1 : 100,
    zIndex12 : 200,
    zIndex3 : 300,
    zIndex4 : 400,
    // 按钮
    btnOrange : "#f4a015",
    btnHoverOrange : "#bd7c11",
    btnBlue : "#025B97",
    btnHoverBlue : "#027BCC"
}
module.exports = globalVars