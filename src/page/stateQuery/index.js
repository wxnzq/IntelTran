/**
 * @author NZQ
 * @data 2018/8/23
 * @Description : deviceManage
 */

// base style
import '@/less/base.less'
import '@/icon/navIconfont/iconfont.css'
//public
import { insertTemplate } from '@/utilJs/public'
//bootstrap
import '@/plugin/bootstrap-3.3.7/css/bootstrap.min.css'
import '@/plugin/bootstrap-3.3.7/js/bootstrap.min'
// 表格插件 -- https://www.cnblogs.com/wdlhao/p/6694083.html --  注意这里的引入顺序,导致表格上的样式不能显示
import "bootstrap-table/dist/bootstrap-table"
import "bootstrap-table/dist/locale/bootstrap-table-zh-CN"
import 'bootstrap-table/dist/bootstrap-table.css'
// layui
import "@/plugin/layer-v2.4.0/layui.all"
import '@/plugin/layer-v2.4.0/css/layui.css'
import '@/plugin/layer-v2.4.0/layer/layer.css' // layer
// 日期插件
import "@/plugin/daterangepicker/js/bootstrap-datepicker"
import "@/plugin/daterangepicker/js/bootstrap-datepicker.zh-CN.min"
import '@/plugin/daterangepicker/css/bootstrap-datepicker.css'
// less 一些样式 或者重写 或者全局
import '@/less/overWrite/layer_over_write.less' // layer 重写的内容
import '@/less/overWrite/bootstrap_over_write.less'
import '@/less/tableRelate.less'
import '@/less/overWrite/bootstrap_table_over_write.less'
import './less/faultQuery.less'
// js
import { faultQuery } from './js/faultQuery'

$(document).ready(function () {
    let leftNacLi = $(".left-nav ul li");
    let contentWrap =  $(".content-wrap");

    // 插入 top nav
    insertTemplate(require('@/components/topNav.html'), $(".top-nav"));
    $("#topNav li:nth-child(5)").addClass("active-blue");

    // 插入 日志管理
    insertTemplate(require('./components/faultQuery.html'), $(".content-wrap"));

    // 左侧导航栏
    layui.use("tree");

    $('#queryDate').datepicker({
        language: 'zh-CN'
    });

    // 调用faultQuery 生成 表格等等
    faultQuery();

})