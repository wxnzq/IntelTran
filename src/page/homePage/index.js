/**
 * @author NZQ
 * @data 2018/8/23
 * @Description : TSCSHomePage
 */
/////////////////////////////css
// base style
import '@/less/base.less'
import '@/icon/navIconfont/iconfont.css'
import '@/plugin/bootstrap-3.3.7/css/bootstrap.min.css'
import '@/plugin/layer-v2.4.0/css/layui.css' // layer
// less 一些样式 或者重写 或者全局
import '@/less/overWrite/layer_over_write.less' // layer 重写的内容
import '@/less/overWrite/bootstrap_over_write.less'
import './less/homePage.less'

/////////////////////////////js
// public
import { insertTemplate } from '@/utilJs/public'
// bootstrap
import '@/plugin/bootstrap-3.3.7/js/bootstrap.min'

// 引入js
import { _map } from './js/map'

insertTemplate(require('@/components/topNav.html'), $(".top-nav"));
$("#topNav li:nth-child(1)").addClass("active-blue");

_map();