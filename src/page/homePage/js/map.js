/**
 * @author NZQ
 * @data 2018/8/28
 * @Description : 地图相关
 * @param: AK z9ZVKtxjuSBByrF2Wsz1I5uZGdsrLEjX
 */


function _map () {
    let map = new BMap.Map("BMap");
    // 泸州市人民政府
    let Luzhou_government =new BMap.Point(105.448463,28.876978);
    // 添加比例尺寸
    let navigation_control = new BMap.NavigationControl({anchor: BMAP_ANCHOR_BOTTOM_RIGHT});
    map.centerAndZoom("泸州",11);
    map.enableScrollWheelZoom(true);

    /*// 右下角控件
    map.addControl(navigation_control);
    // 街区选择控件
    map.addControl(new BMap.CityListControl({
        anchor: BMAP_ANCHOR_TOP_LEFT,
        offset: new BMap.Size(10, 20),
    }));*/
}

export { _map }
