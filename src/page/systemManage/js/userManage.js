/**
 * @author NZQ
 * @data 2018/8/30
 * @Description : 用户管理
 */

import { modalHandle } from '../../../assets/utilJs/modal'

function userManage () {
    let table = $("#Table")
    let data = [
        {
            id : "002",
            name : "用户名称",
            workUnit : "工作单位",
            state : 0
        },
        {
            id : "001",
            name : "用户名称用户名称用户名称用户名称用户名称用户名称用户名称用户名称用户名称用户名称用户名称用户名称用户名称用户名称用户名称",
            workUnit : "工作单位",
            state : 1
        },
        {
            id : "002",
            name : "用户名称",
            workUnit : "工作单位",
            state : 0
        },
        {
            id : "002",
            name : "用户名称",
            workUnit : "工作单位",
            state : 1
        },
    ];
    let columns = [
        {
            checkbox : true
        },
        {
            field: 'id',
            title: '编号',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '15%'
        },
        {
            field: 'name',
            title: '用户名称',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '30%'
        },
        {
            field: 'workUnit',
            title: '工作单位',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '10%'
        },
        {
            field: 'state',
            title: '状态',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '10%',
            formatter : function (value) {
                if (value === 1) {
                    return `<p class="td-state-normal">在线</p>`
                }else if (value === 0) {
                    return `<p class="td-state-not-normal">离线</p>`
                }
            }
        },
        {
            title: '编辑',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '15%',
            formatter : function () {
                return `<button type="button" class="btn btn-query btn-modify">修改</button>`
            }
        },
        {
            title: '操作',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '20%',
            formatter : function () {
                return `<button class="btn btn-query btn-check">查看</button><button class="btn btn-delete">重置密码</button>`
            }
        },
    ];

    // 修改按钮
    function modifyUser () {
        modalHandle($(".modify-add-modal"), $(".btn-modify"), $(".modify-modal .close"), "修改用户");
    }

    // 新增用户
    function addUser () {
        modalHandle($(".modify-add-modal"), $(".btn-increase"), $(".modify-modal .close"), "新增用户");
    }

    // 查看用户
    function checkUser () {
        modalHandle($(".check-modal"), $(".btn-check"), $(".check-modal .close"), "查看用户");
    }



    // 异步调用 打包 生成table
    import(/* webpackChunkName : "createTable" */ '../../../assets/utilJs/table').then(module => {
        module.createTable({
            elem : table,
            columns : columns ,
            data : data
        })
    }).then(function () {
        modifyUser();
        checkUser();
        addUser();
    })

}

export { userManage }