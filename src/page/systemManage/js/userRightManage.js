/**
 * @author NZQ
 * @data 2018/8/30
 * @Description : 权限管理
 */

import '../less/userRight.less'
import { modalHandle } from '../../../assets/utilJs/modal'

function userRightManage () {
    let data = [
        {
            id : "001",
            name : "权限名称",
            describe : "权限描述",
            time : "创建时间"
        },
    ];
    let columns = [
        {
            checkbox : true
        },
        {
            field: 'id',
            title: '编号',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '15%'
        },
        {
            field: 'name',
            title: '权限名称',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '30%'
        },
        {
            field: 'describe',
            title: '权限描述',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '10%'
        },
        {
            field: 'time',
            title: '创建时间',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '10%'
        },
        {
            title: '操作',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '20%',
            formatter : function () {
                return `<button class="btn btn-query btn-modify">修改</button><button class="btn btn-delete">删除</button>`
            }
        },
    ];
    // 新增加
    function addUserRight() {
        modalHandle($("#userRight"), $(".btn-increase"), $("#userRight .close"), "添加权限");
    }
    // 修改按钮
    function modifyUserRight () {
        modalHandle($("#userRight"), $(".btn-modify"), $("#userRight .close"), "修改权限");
    }



    // 异步调用 打包 生成table
    import(/* webpackChunkName : "createTable" */ '../../../assets/utilJs/table').then(module => {
        module.createTable({
            elem : $("#Table"),
            columns : columns ,
            data : data
        })
    }).then(function () {
        modifyUserRight();
        addUserRight();
    })
}

export { userRightManage }