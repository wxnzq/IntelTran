/**
 * @author NZQ
 * @data 2018/8/30
 * @Description : 用户组管理
 */


import { modalHandle } from '../../../assets/utilJs/modal'

function userGroupManage () {
    let data = [
        {
            id : "002",
            name : "用户名称",
            workUnit : "工作单位",
            state : 0
        }
    ];
    let columns = [
        {
            checkbox : true
        },
        {
            field: 'id',
            title: '编号',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '15%'
        },
        {
            field: 'name',
            title: '用户组名称',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '20%'
        },
        {
            field: 'workUnit',
            title: '用户组权限',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '20%'
        },
        {
            field: 'nowNum',
            title: '现有人数',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '10%'
        },
        {
            field : "maxNum",
            title: '最大容纳人数',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '15%'
        },
        {
            title: '操作',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '20%',
            formatter : function () {
                return `<button class="btn btn-query btn-modify">修改</button><button class="btn btn-delete">删除</button>`
            }
        },
    ];

    // 新增加
    function addUserGroup () {
        modalHandle($("#userGroup"), $(".btn-increase"), $("#userGroup .close"), "新增用户组");
    }
    // 修改按钮
    function modifyUserGroup () {
        modalHandle($("#userGroup"), $(".btn-modify"), $("#userGroup .close"), "修改用户组");
    }


    // 异步调用 打包 生成table
    import(/* webpackChunkName : "createTable" */ '../../../assets/utilJs/table').then(module => {
        module.createTable({
            elem : $("#Table"),
            columns : columns ,
            data : data
        })
    }).then(function () {
        modifyUserGroup();
        addUserGroup();
    })

}

export { userGroupManage }