/**
 * @author NZQ
 * @data 2018/8/30
 * @Description : 特勤任务 信息
 */

// 思路：先创建layer  创建完成后 立即请求数据 完成渲染
import '../less/missionInfo.less'
import { modalHandle, openModal, closeModal } from '../../../assets/utilJs/modal'

function missionInfo () {
    // 创建设备管理的表格
    let data = [
        {
            id: "001",
            name : "nzq",
            state : 0
        },
        {
            id: "001",
            name : "nzq",
            state : 1
        },
        {
            id: "001",
            name : "nzq",
            state : 2
        },
        {
            id: "001",
            name : "nzq",
            state : 0
        },
        {
            id: "001",
            name : "nzq",
            state : 1
        },
        {
            id: "001",
            name : "nzq",
            state : 2
        },
        {
            id: "001",
            name : "nzq",
            state : 0
        },
        {
            id: "001",
            name : "nzq",
            state : 1
        },
        {
            id: "001",
            name : "nzq",
            state : 2
        },
        {
            id: "001",
            name : "nzq",
            state : 0
        },
        {
            id: "001",
            name : "nzq",
            state : 1
        },
        {
            id: "001",
            name : "nzq",
            state : 2
        },
        {
            id: "001",
            name : "nzq",
            state : 0
        },
        {
            id: "001",
            name : "nzq",
            state : 1
        },
        {
            id: "001",
            name : "nzq",
            state : 2
        },
        {
            id: "001",
            name : "nzq",
            state : 0
        },
        {
            id: "001",
            name : "nzq",
            state : 1
        },
        {
            id: "001",
            name : "nzq",
            state : 2
        },
        {
            id: "001",
            name : "nzq",
            state : 0
        },
        {
            id: "001",
            name : "nzq",
            state : 1
        },
        {
            id: "001",
            name : "nzq",
            state : 2
        },
        {
            id: "001",
            name : "nzq",
            state : 0
        },
        {
            id: "001",
            name : "nzq",
            state : 1
        },
        {
            id: "001",
            name : "nzq",
            state : 2
        },
        {
            id: "001",
            name : "nzq",
            state : 0
        },
        {
            id: "001",
            name : "nzq",
            state : 1
        },
        {
            id: "001",
            name : "nzq",
            state : 2
        },
        {
            id: "001",
            name : "nzq",
            state : 0
        },
        {
            id: "001",
            name : "nzq",
            state : 1
        },
        {
            id: "001",
            name : "nzq",
            state : 2
        },
        {
            id: "001",
            name : "nzq",
            state : 0
        },
        {
            id: "001",
            name : "nzq",
            state : 1
        },
        {
            id: "001",
            name : "nzq",
            state : 2
        }
    ];
    let columns = [
        {
            checkbox : true
        },
        {
            field: 'id',
            title: '特勤路线ID',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '20%'
        },
        {
            field: 'name',
            title: '特勤任务名称',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '30%'
        },
        {
            field: 'state',
            title: '特勤状态',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '25%',
            formatter : function (state) {
                if (state === 0) {
                    return `<p style="color: gray;">未执行</p>`
                }else if (state === 1) {
                    return `<p style="color: orange;">预执行</p>`
                }else if (state === 2) {
                    return `<p style="color: red;">正在执行</p>`
                }
            }
        },
        {
            title: '操作',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '25%',
            formatter : function () {
                return `<button type="button" class="btn  btn-query">查看</button>
                        <button type="button" class="btn  btn-delete btn-modify">修改</button>`
            }
        }
    ];

    // 查看特勤信息
    function missionCheck () {
        modalHandle($("#missionCheck"), $("#Table .btn-query"), $("#missionCheck .close"), "特情信息");
        manageControl();
    }
    // 修改特勤信息
    function missionModify () {
        modalHandle($("#missionHandle"), $("#Table .btn-modify"), $("#missionHandle .close"), "修改信息")
    }
    // 新增
    function missionAdd () {
        modalHandle($("#missionHandle"), $(".table-nav .btn-add"), $("#missionHandle .close"), "新增信息")
    }
    // 强制管控 -- 从查看中打开
    function manageControl () {
        modalHandle($("#manageControl"), $("#missionCheck .btn-manage"), $("#manageControl .close"), "强制管控")
    }
    // 异步调用 打包 生成table
    import(/* webpackChunkName : "createTable" */ '../../../assets/utilJs/table').then(module => {
        module.createTable({
            elem : $("#Table"),
            columns : columns ,
            data : data
        })
    }).then(function () {
       missionCheck();
       missionModify();
       missionAdd();
    })
}

export { missionInfo }