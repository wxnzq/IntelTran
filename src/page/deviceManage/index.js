/**
 * @author NZQ
 * @data 2018/8/23
 * @Description : deviceManage
 */

////////////////////////////css
// base style
import '@/less/base.less'
import '@/icon/navIconfont/iconfont.css'
import '@/plugin/bootstrap-3.3.7/css/bootstrap.min.css'
import 'bootstrap-table/dist/bootstrap-table.css'
import '@/plugin/layer-v2.4.0/css/layui.css'
import '@/plugin/layer-v2.4.0/layer/layer.css' // layer
// less 一些样式 或者重写 或者全局
import '@/less/overWrite/layer_over_write.less' // layer 重写的内容
import '@/less/overWrite/bootstrap_over_write.less'
import '@/less/tableRelate.less'
import '@/less/overWrite/bootstrap_table_over_write.less'
import './less/deviceHandle.less'

////////////////////////////js
//bootstrap
import '@/plugin/bootstrap-3.3.7/js/bootstrap.min'
// 表格插件 -- https://www.cnblogs.com/wdlhao/p/6694083.html --  注意这里的引入顺序,导致表格上的样式不能显示
import "bootstrap-table/dist/bootstrap-table"
import "bootstrap-table/dist/locale/bootstrap-table-zh-CN"
// layui
import "@/plugin/layer-v2.4.0/layui.all"
//public
import { Nav, insertTemplate } from '@/utilJs/public'
//各个模块 js
import { deviceHandle } from './js/deviceHandle'
import { loadEditor } from './js/loadEditor'
import { schemeConfig } from './js/schemeConfig'

$(document).ready(function () {
    // 插入 top nav
    insertTemplate(require('@/components/topNav.html'), $(".top-nav"));
    $("#topNav li:nth-child(3)").addClass("active-blue");
    // 左侧导航栏
    layui.use("tree");

    // 插入 设备管理
    insertTemplate(require('./components/deviceHandle.html'), $(".content-wrap"));

    // 调用deviceHandle 生成 表格等等
    deviceHandle();
    // 具体写法参考   public.js
    new Nav("deviceManage",{
        deviceHandle : deviceHandle,
        loadEditor : loadEditor,
        schemeConfig : schemeConfig
    })
})