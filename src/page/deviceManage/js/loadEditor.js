/**
 * @author NZQ
 * @data 2018/8/30
 * @Description : 路口编辑
 */

import '../less/loadEditor.less'
import { modalHandle } from '../../../assets/utilJs/modal'

/**
 * @author NZQ
 * @data 2018/8/30
 * @Description : 设备管理
 */
// 思路：先创建layer  创建完成后 立即请求数据 完成渲染


function loadEditor () {

    // 创建设备管理的表格
    let data = [
        {
            id : "001",
            name : "成都信息工程大学",
            model : "四字路口"
        },
        {
            id : "001",
            name : "成都信息工程大学",
            model : "四字路口"
        },
        {
            id : "001",
            name : "成都信息工程大学",
            model : "四字路口"
        },
        {
            id : "001",
            name : "成都信息工程大学",
            model : "四字路口"
        },
        {
            id : "001",
            name : "成都信息工程大学",
            model : "四字路口"
        },{
            id : "001",
            name : "成都信息工程大学",
            model : "四字路口"
        },
        {
            id : "001",
            name : "成都信息工程大学",
            model : "四字路口"
        },
        {
            id : "001",
            name : "成都信息工程大学",
            model : "四字路口"
        },
        {
            id : "001",
            name : "成都信息工程大学",
            model : "四字路口"
        },
        {
            id : "001",
            name : "成都信息工程大学",
            model : "四字路口"
        },
        {
            id : "001",
            name : "成都信息工程大学",
            model : "四字路口"
        },
        {
            id : "001",
            name : "成都信息工程大学",
            model : "四字路口"
        },
        {
            id : "001",
            name : "成都信息工程大学",
            model : "四字路口"
        },{
            id : "001",
            name : "成都信息工程大学",
            model : "四字路口"
        },
        {
            id : "001",
            name : "成都信息工程大学",
            model : "四字路口"
        },
        {
            id : "001",
            name : "成都信息工程大学",
            model : "四字路口"
        },
        {
            id : "001",
            name : "成都信息工程大学",
            model : "四字路口"
        },
        {
            id : "001",
            name : "成都信息工程大学",
            model : "四字路口"
        },
        {
            id : "001",
            name : "成都信息工程大学",
            model : "四字路口"
        },
        {
            id : "001",
            name : "成都信息工程大学",
            model : "四字路口"
        },
        {
            id : "001",
            name : "成都信息工程大学",
            model : "四字路口"
        },{
            id : "001",
            name : "成都信息工程大学",
            model : "四字路口"
        },
        {
            id : "001",
            name : "成都信息工程大学",
            model : "四字路口"
        },
        {
            id : "001",
            name : "成都信息工程大学",
            model : "四字路口"
        }

    ];
    let columns = [
        {
            checkbox : true
        },
        {
            field: 'id',
            title: '路口编号',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '20%'
        },
        {
            field: 'name',
            title: '路口名称',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '30%'
        },
        {
            field: 'model',
            title: '模型',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '30%',
        },
        {
            title: '操作',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            formatter : function () {
              return `<button class="btn btn-query" id="bootstrap-button ">查看</button>
                        <button type="button" class="btn  btn-delete btn-modify">修改</button>`
            },
            width: '20%'
        }
    ];

    // 点击表格中的查看，生成弹窗
    function checkLoad () {
        let loadName = "路口名称"
        modalHandle($("#loadCheck"), $("#Table .btn-query"), $("#loadCheck .close"), loadName);
    }
    // 修改信息
    function modifyLoad () {
        modalHandle($("#loadHandle"), $("#Table .btn-modify"), $("#loadHandle .close"), "修改路口")
    }
    // 新增加控制机
    function addLoad () {
        modalHandle($("#loadHandle"), $(".table-nav .btn-add"), $("#loadHandle .close"), "新增路口")
    }

    // 异步调用 打包 生成table
    import(/* webpackChunkName : "createTable" */ '../../../assets/utilJs/table').then(module => {
        module.createTable({
            elem : $("#Table"),
            columns : columns ,
            data : data
        })
    }).then(function () {
        checkLoad();
        modifyLoad();
        addLoad();
    })
}

export { loadEditor }


/*
*   想这个的话  不要动文件   可以注释掉 data

    // 异步调用 打包 生成table
    import( webpackChunkName : "createTable" '../../../assets/utilJs/table').then(module => {
    module.createTable({
        elem : $("#Table"),
        columns : columns ,
        data : data
    })

    ======>>>> 换成 function(data){
         // 异步调用 打包 生成table
            import( webpackChunkName : "createTable" '../../../assets/utilJs/table').then(module => {
                module.createTable({
                    elem : $("#Table"),
                    columns : columns ,
                    data : data
                })
            }
            放入
            new AjaxRequest()中 (这个配置  参考前面)

}).then(function () {
    checkLoad();
    modifyLoad();
    addLoad();
})
}

*
* */