/**
 * @author NZQ
 * @data 2018/8/30
 * @Description : 方案配置
 */
import '../less/schemeConfig.less'
import { modalHandle } from '../../../assets/utilJs/modal'


function schemeConfig () {
    // 创建设备管理的表格
    let data = [
        {
            id : "0001",
            name : "方案名称",
            state : 0
        },
        {
            id : "0001",
            name : "方案名称",
            state : 1
        },
        {
            id : "0001",
            name : "方案名称",
            state : 2
        },
        {
            id : "0001",
            name : "方案名称",
            state : 2
        },
        {
            id : "0001",
            name : "方案名称",
            state : 0
        },
        {
            id : "0001",
            name : "方案名称",
            state : 0
        },
        {
            id : "0001",
            name : "方案名称",
            state : 1
        },
        {
            id : "0001",
            name : "方案名称",
            state : 2
        },
        {
            id : "0001",
            name : "方案名称",
            state : 2
        },
        {
            id : "0001",
            name : "方案名称",
            state : 0
        },
        {
            id : "0001",
            name : "方案名称",
            state : 0
        },
        {
            id : "0001",
            name : "方案名称",
            state : 1
        },
        {
            id : "0001",
            name : "方案名称",
            state : 2
        },
        {
            id : "0001",
            name : "方案名称",
            state : 2
        },
        {
            id : "0001",
            name : "方案名称",
            state : 0
        },
        {
            id : "0001",
            name : "方案名称",
            state : 0
        },
        {
            id : "0001",
            name : "方案名称",
            state : 1
        },
        {
            id : "0001",
            name : "方案名称",
            state : 2
        },
        {
            id : "0001",
            name : "方案名称",
            state : 2
        },
        {
            id : "0001",
            name : "方案名称",
            state : 0
        },
        {
            id : "0001",
            name : "方案名称",
            state : 0
        },
        {
            id : "0001",
            name : "方案名称",
            state : 1
        },
        {
            id : "0001",
            name : "方案名称",
            state : 2
        },
        {
            id : "0001",
            name : "方案名称",
            state : 2
        },
        {
            id : "0001",
            name : "方案名称",
            state : 0
        },
        {
            id : "0001",
            name : "方案名称",
            state : 0
        },
        {
            id : "0001",
            name : "方案名称",
            state : 1
        },
        {
            id : "0001",
            name : "方案名称",
            state : 2
        },
        {
            id : "0001",
            name : "方案名称",
            state : 2
        },
        {
            id : "0001",
            name : "方案名称",
            state : 0
        }
    ];
    let columns = [
        {
            checkbox : true
        },
        {
            field: 'id',
            title: '方案编号',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '20%'
        },
        {
            field: 'name',
            title: '方案名称',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '30%'
        },
        {
            field: 'state',
            title: '执行状态',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            width: '30%',
            formatter : function (state) {
                if (state === 0) {
                    return `<p style="color: gray;">未执行</p>`
                }else if (state === 1) {
                    return `<p style="color: orange;">预执行</p>`
                }else if (state === 2) {
                    return `<p style="color: red;">正在执行</p>`
                }
            }
        },
        {
            title: '操作',
            align: 'center',
            halign: 'center',
            valign: 'middle',
            formatter : function () {
                return `<button class="btn btn-query" id="bootstrap-button ">查看</button>
                        <button type="button" class="btn  btn-delete btn-modify">修改</button>`
            },
            width: '20%'
        }
    ];

    // 点击表格中的查看，生成弹窗
    function checkScheme () {
        let schemeName = "方案名称"
        modalHandle($("#schemeCheck"), $("#Table .btn-query"), $("#schemeCheck .close"), schemeName);
    }
    // 修改信息
    function modifyScheme() {
        modalHandle($("#schemeHandle"), $("#Table .btn-modify"), $("#schemeHandle .close"), "修改方案")
    }
    // 新增加控制机
    function addScheme () {
        modalHandle($("#schemeHandle"), $(".table-nav .btn-add"), $("#schemeHandle .close"), "新增方案")
    }

    // 异步调用 打包 生成table
    import(/* webpackChunkName : "createTable" */ '../../../assets/utilJs/table').then(module => {
        module.createTable({
            elem : $("#Table"),
            columns : columns ,
            data : data
        })
    }).then(function () {
       addScheme();
       modifyScheme();
       checkScheme();
    })
}

export { schemeConfig }