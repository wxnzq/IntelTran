/**
 * @author NZQ
 * @data 2018/8/23
 * @Description : specialMission
 */

//////////////////////////////////css
// base style
import '@/less/base.less'
import '@/icon/navIconfont/iconfont.css'
import '@/plugin/bootstrap-3.3.7/css/bootstrap.min.css'
import 'bootstrap-table/dist/bootstrap-table.css'
import '@/plugin/layer-v2.4.0/css/layui.css'
import '@/plugin/layer-v2.4.0/layer/layer.css' // layer
// less 一些样式 或者重写 或者全局
import '@/less/overWrite/layer_over_write.less' // layer 重写的内容
import '@/less/overWrite/bootstrap_over_write.less'
import '@/less/tableRelate.less'
import '@/less/overWrite/bootstrap_table_over_write.less'
import './less/singlePoint.less'
import './less/timeControl.less'
import './less/phaseControl.less'
import './less/diretionControl.less'
//////////////////////////////////js
// public
import { insertTemplate, Nav } from '@/utilJs/public'
// bootstrap
import '@/plugin/bootstrap-3.3.7/js/bootstrap.min'
// 表格插件 -- https://www.cnblogs.com/wdlhao/p/6694083.html --  注意这里的引入顺序,导致表格上的样式不能显示
import "bootstrap-table/dist/bootstrap-table"
import "bootstrap-table/dist/locale/bootstrap-table-zh-CN"
// layui
import "@/plugin/layer-v2.4.0/layui.all"

$(document).ready(function () {
    let leftNacLi = $(".left-nav ul li");
    let contentWrap =  $(".content-wrap");

    // 插入 top nav
    insertTemplate(require('@/components/topNav.html'), $(".top-nav"));
    $("#topNav li:nth-child(2)").addClass("active-blue");

    // 左侧导航栏
    layui.use("tree");

    // 插入 控制详情
    insertTemplate(require('./components/controlDetail.html'), contentWrap);
    import(/* webpackChunkName : "controlDetail" */ './js/controlDetail').then(module => {
            // 这里只能原生的代码 获取节点
            module.createECharts(document.querySelector("#EChartCircle"))
    })

    new Nav("singlePoint",{
        controlDetail : function () {
            import(/* webpackChunkName : "controlDetail" */ './js/controlDetail').then(module => {
                // 这里只能原生的代码 获取节点
                module.createECharts(document.querySelector("#EChartCircle"))
            })
        }
    })

})