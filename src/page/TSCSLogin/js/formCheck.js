/**
 * @author NZQ
 * @data 2018/8/27
 * @Description : 表单验证相关
 */

import '../../../assets/utilJs/public'
import { ajax } from '../../../assets/utilJs/public'
ajax();
// AjaxRequest(中填入值) 把这个东西放在适当的地方就可以使用，  记住里面参数自己配置  参考public.js

export default function checkForm(userEle, passEle) {
    let username = userEle.val();
    let password = passEle.val();

    // username
    if (username === ''){
        layer.open({
            title: '输入错误',
            content: '账号不能为空！'
        });
        return false
    }else if (!(/^\d{10}$/.test(username))) {
        layer.open({
            title: '输入错误',
            content: '账号格式错误！'
        });
        return false
    }


    // password
    if (username === ""){
        layer.open({
            title: '输入错误',
            content: '密码不能为空！'
        });
        return false
    }else if (!(/^[a-zA-Z0-9]{6,20}$/.test(password))) {
        layer.open({
            title: '输入错误',
            content: '密码格式错误！'
        });
        return false
    }

    return true;
}

