/**
 * @author NZQ
 * @data 2018/8/23
 * @Description : TSCSLogin
 */

//////////////////////////////////////css
// base style
import '@/less/base.less'
// bootstrap
import '@/plugin/bootstrap-3.3.7/css/bootstrap.min.css'
// layui
import '@/plugin/layer-v2.4.0/layer/layer.css' // layer
// less 一些样式 或者重写 或者全局
import './less/login.less'
import '@/less/overWrite/layer_over_write.less' // layer 重写的内容
//////////////////////////////////////js
import '@/plugin/layer-v2.4.0/layer/layer' // layer -- 在checkForm中被使用
// 表单检测
import checkForm from './js/formCheck' // 不用异步

let username = $("#username");
let password = $("#password");
let submitBtn = $("#submitBtn");


// 表单验证
submitBtn.click(function () {
    if (checkForm(username, password)) {
        // 执行submit相关的东西
        window.location.href = "./homePage.html"
    }else{

    }
})
