/**
 * @author NZQ
 * @date 2018/8/22
 * @Description : 实现less语言在编译时补全css代码的兼容性前缀
*/
// https://github.com/postcss/autoprefixer
let autoprefixer = require('autoprefixer');

module.exports = {
    plugins: [
        autoprefixer({
            browsers: [
                // 加这个后可以出现额外的兼容性前缀
                "> 0.01%"
            ]
        })
    ]
}
